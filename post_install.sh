#!/bin/bash

SHELL="/bin/bash"

if ! grep "${SHELL}" "$BPM_ROOT"/etc/shells; then
  echo "${SHELL}" | tee -a "$BPM_ROOT"/etc/shells
fi
