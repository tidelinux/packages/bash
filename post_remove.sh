#!/bin/bash

SHELL="/bin/bash"

if grep "${SHELL}" "$BPM_ROOT"/etc/shells; then
  sed -i "\|${SHELL}|d" "$BPM_ROOT"/etc/shells
fi
