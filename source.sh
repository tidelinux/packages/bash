# This is the source.sh script. It is executed by BPM in a temporary directory when compiling a source package
# BPM Expects the source code to be extracted into the automatically created 'source' directory which can be accessed using $BPM_SOURCE
# BPM Expects the output files to be present in the automatically created 'output' directory which can be accessed using $BPM_OUTPUT

DOWNLOAD="https://ftp.gnu.org/gnu/bash/bash-${BPM_PKG_VERSION}.tar.gz"
FILENAME="${DOWNLOAD##*/}"

# The prepare function is executed in the root of the temp directory
# This function is used for downloading files and putting them into the correct location
prepare() {
  wget "$DOWNLOAD"
  tar -xvf "$FILENAME" --strip-components=1 -C "$BPM_SOURCE"
}

# The build function is executed in the source directory
# This function is used to compile the source code
build() {
  ./configure --prefix=/usr             \
            --without-bash-malloc     \
            --with-installed-readline \
            --docdir=/usr/share/doc/bash
  make
}

# The check function is executed in the source directory
# This function is used to run tests to verify the package has been compiled correctly
check() {
  make check
}

# The package function is executed in the source directory
# This function is used to move the compiled files into the output directory
package() {
  make DESTDIR="$BPM_OUTPUT" install
  ln -s bash "$BPM_OUTPUT"/usr/bin/sh
  mkdir -p "$BPM_OUTPUT"/etc
  cp ../profile "$BPM_OUTPUT"/etc/profile
  cp ../bashrc "$BPM_OUTPUT"/etc/bashrc
}
